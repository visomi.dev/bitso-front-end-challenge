import middy from '@middy/core';
import cors from '@middy/http-cors';

const handlerResolver = (context: string) => {
  return `${context.split(process.cwd())[1].substring(1).replace(/\\/g, '/')}`;
};

const proxy = {
  handler: `${handlerResolver(__dirname)}/handlers.proxy`,
  events: [
    {
      http: {
        path: '**',
        method: 'all',
        cors: true,
      },
    },
  ],
};

const lambdas = { proxy };

export default lambdas;
