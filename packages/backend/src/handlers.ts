import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import 'source-map-support/register';

import type { MiddyfiedHandler } from '@middy/core';

export const proxy: MiddyfiedHandler<
  APIGatewayProxyEvent
> = middyfy(async (event: APIGatewayProxyEvent): APIGatewayProxyResult => {

});
