/* eslint-disable no-template-curly-in-string */

import type { AWS } from '@serverless/typescript';

import lambdas from './src';

const serverlessConfiguration: AWS = {
  service: 'users',
  frameworkVersion: '2',
  custom: {
    webpack: {
      webpackConfig: './webpack.config.js',
      includeModules: true,
    },
  },
  plugins: ['serverless-webpack', 'serverless-offline'],
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      DATABASE_URL: '${env:DATABASE_URL}',
    },
    lambdaHashingVersion: '20201221',
    stage: 'dev',
    region: 'us-west-1',
  },
  functions: lambdas,
  package: {
    individually: true,
  },
  useDotenv: true,
};

export default serverlessConfiguration;
