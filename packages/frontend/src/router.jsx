import React from 'react'
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'

import Exchange from './views/Exchange'

/**
 * This component provides the available routes to navigate in
 */
const Router = () => (
  <BrowserRouter>
    <Switch>
      <Redirect name='home' from='/' to='/exchange/btc-mxn' exact  />
      <Route name='exchange' path='/exchange/:book([a-z]{3}-[a-z]{3})' component={(props) => <Exchange page='exchange' {...props} />} />
    </Switch>
  </BrowserRouter>
)

export default Router
