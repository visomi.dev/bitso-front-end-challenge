import React from 'react'

const DEFAULT_PERIOD = '1year'

export default Object.freeze({
  chartOptions: [
    {
      value: 'candlestick',
      label: <img src='/static/images/1x/icon_candles.png' alt='candlestick-chart' />
    },
    {
      value: 'deep',
      label: <img src='/static/images/1x/icon_deep.png' alt='deep-chart' />
    }
  ],

  periodOptions: [
    {
      value: '1month',
      label: '1m'
    },
    {
      value: '3months',
      label: '3m'
    },
    {
      value: DEFAULT_PERIOD,
      label: '1y'
    }
  ],

  intervalOptions: [
    {
      value: '1hour',
      label: '1h'
    },
    {
      value: '5hours',
      label: '5h'
    },
    {
      value: '1minute',
      label: '1m'
    }
  ],

  defaultPeriod: DEFAULT_PERIOD
})
