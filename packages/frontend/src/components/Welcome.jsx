import React, { Component } from 'react'
import classnames from 'classnames'

class Welcome extends Component {
  constructor (props) {
    super(props)
    this.state = {
      active: true
    }
  }

  continue () {
    this.setState({ active: false })
  }

  render () {
    return (
      <div className={classnames('modal welcome', { 'is-active': this.state.active })}>
        <div className='modal-background' />
        <div className='modal-content'>
          <img src='/static/images/bann_bfec.jpg' alt='bann_bfec' />
          <h1>WebApp by visomi</h1>
          <p>
            this app was made with React, Redux and CSS3 with flexbox
          </p>
          <button onClick={this.continue.bind(this)}>
            continue
          </button>
        </div>
      </div>
    )
  }
}

export default Welcome
