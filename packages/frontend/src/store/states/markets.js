const state = {
  list: [],
  loading: false,
  error: false,
  errorMessage: ''
};

export default state;
