const state = {
  headerSidebar: false,
  marketsSidebar: false,
  loading: false,
  error: false,
  errorMessage: '',
  theme: 'dark'
};

export default state;
