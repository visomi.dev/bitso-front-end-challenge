const state = {
  updatedAt: '',
  bids: [],
  asks: [],
  sequence: '',
  loading: false
};

export default state;
