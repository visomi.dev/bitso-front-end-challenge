const state = {
  data: [],
  timeline: [],
  current: {
    book: '',
    volume: '',
    high: '',
    last: '',
    createdAt: '',
    vwap: '',
    low: '',
    ask: 0,
    bid: 0
  },
  loading: false,
  error: false,
  errorMessage: ''
};

export default state;
