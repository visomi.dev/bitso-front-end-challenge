import { handleActions } from 'redux-actions'

import initialState from '../states/trades'

export default handleActions({
  SET_LATEST_TRADES (state, action) {
    return { ...state, latest: action.payload }
  },

  CLEAR_TRADES_DATA (state, action) {
    return { ...initialState }
  },

  SET_TRADES_LOADING (state, action) {
    return { ...state, loading: action.payload }
  }
}, initialState)
