import { handleActions } from 'redux-actions'

import initialState from '../states/books'

export default handleActions({
  SET_BOOKS_LIST (state, action) {
    return { ...state, list: action.payload }
  },

  CLEAR_BOOKS_DATA (state, action) {
    return { ...initialState }
  },

  SET_BOOKS_LOADING (state, action) {
    return { ...state, loading: action.payload }
  }
}, initialState)
