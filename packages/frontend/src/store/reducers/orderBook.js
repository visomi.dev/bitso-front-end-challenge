import { handleActions } from 'redux-actions'

import initialState from '../states/orderBook'

export default handleActions({
  SET_ORDER_BOOK_DATA (state, action) {
    return { ...state, ...action.payload }
  },

  CLEAR_ORDER_BOOK_DATA (state, action) {
    return { ...initialState }
  },

  SET_ORDER_BOOK_LOADING (state, action) {
    return { ...state, loading: action.payload }
  }
}, initialState)
